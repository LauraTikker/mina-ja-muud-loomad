﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom kroko = new Loom("krokodill");
            Console.WriteLine(kroko);
            kroko.TeeHäält();
            Koduloom kodune = new Koduloom("prussakas", "mati");
            kodune.Nimi = "Pati";
            Console.WriteLine(kodune);
            kodune.TeeHäält();
            Console.WriteLine("mõned asjad veel\n");
            List<Loom> loomad = new List<Loom>(); // siia saab panna kakoduloomad, sest nad on ka loomad

            loomad.Add(kroko);
            loomad.Add(kodune);

            foreach (var x in loomad) Console.WriteLine(x);

            Koer k1 = new Koer("Pontu") { Tõug = "taks", };
            k1.TeeHäält();
            Kass k2 = new Kass("Miisu") { Tõug = "angoora" };
            k2.Silita();
            k2.TeeHäält();


        }

        //Koduloom on nii koduloom kui ka loom

        class Loom // baasklass, millest on tuletatud mõni teine
        {
            public string Liik; //väli

            public Loom(string liik = "tundmatu") { Liik = liik; } //kontruktor

            public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält"); //virtual: saab tuletatud klassis ümberdefineerida

            public override string ToString() => $"on loom liigist {Liik}";
        }

        class Koduloom : Loom // koduloom on tuletatud klass, Loom on baasklass. Koduloom saab baasklassist kõik kaasa vä kontruktor
        {
            public string Nimi; // lisatud väli - seda baasklassil ei ole

            public Koduloom(string liik, string nimi = "nimeveelpole") : base(liik) //kontruktor. kutsub välja baseklassi kontruktori: konstruktori sidumine
            { Nimi = nimi; }

            public override string ToString() => $"{Nimi} on {Liik}";

            public override void TeeHäält() // baasklassi meetod ümberdefineeritult
            {
                Console.WriteLine($"{Nimi} teeb mahedat häält (ta on {Liik})");
            }
        }

        class Kass : Koduloom
        {
            public string Tõug = "segavereline";
            public Kass (string nimi) : base("kass", nimi) { } //määrab alguses liigi (vajalik baasklassi kontruktoris ja paneb ka nime)

            bool tuju = false;

            public void SikutaSabast() => tuju = false;
            public void Silita() => tuju = true;

            public override void TeeHäält()
            {
                if (tuju) Console.WriteLine($"{Tõug} kass {Nimi} lööb mõnusasti nurru");
                else Console.WriteLine($"{Nimi} kräunub");
            }
            

        }

        class Koer : Koduloom //ilma parameetriteta kodulooma ei saa, peab pöörduma baasklassi poole
        {
            public string Tõug = "krants";
            public Koer(string nimi) : base("koer", nimi) { } // sulud näitavad, et me ei tee seal midagi
            public override void TeeHäält()
            {
                Console.WriteLine($"{Nimi} haugub ja tema tõug on {Tõug}");
            }
        }
    }

    
}
